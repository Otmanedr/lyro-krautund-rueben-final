
/*Auswahl eines Rezept nach rezeptname */
SELECT * FROM rezept WHERE re_name LIKE 'Lachs%' ;

/*Auswahl aller Rezepte eine Ernährungskategorie*/
SELECT R.re_name, R.re_anleitung 
FROM rezept AS R INNER JOIN  ernaehrungskategorie_rez AS ER ON R.re_id= ER.rezept_id
WHERE ER.ernkat_id=2; 



/*Auswahl aller Rezepte die eine gewisse Zutat enthalten*/

SELECT R.re_name,R.re_anleitung
FROM rezept AS R INNER JOIN zutat_rezept AS ZR ON R.re_id=ZR.rezept_id 
INNER JOIN zutat AS Z ON ZR.zutat_id= Z.ZUTATENNR
WHERE zutat_id=3001;

/*Berechnung der durchschnittlichen Nährwerte aller Bestellungen eines Kunden*/

SELECT AVG(Z.KALORIEN) 
FROM kunde AS K INNER JOIN bestellung AS B ON K.KUNDENNR= B.KUNDENNR 
 join bestellungzutat AS BT ON B.BESTELLNR= BT.BESTELLNR
 JOIN zutat AS Z ON Z.ZUTATENNR=BT.ZUTATENNR
 WHERE K.KUNDENNR= 2001;

/*Auswahl aller Zutaten, die bisher keinem Rezept zugeordnet sind*/

SELECT Z.ZUTATENNR , Z.BEZEICHNUNG, Z.EINHEIT,Z.NETTOPREIS ,Z.KALORIEN , Z.KALORIEN,Z.KOHLENHYDRATE,Z.PROTEIN
FROM zutat AS Z LEFT JOIN zutat_rezept AS ZT ON Z.ZUTATENNR= ZT.zutat_id
WHERE ZT.zutat_id IS NULL;
 
/*Auswahl aller Rezepte, die eine bestimmte Kalorienmenge nicht überschreiten */
SELECT R.re_name, R.re_anleitung, SUM(Z.KALORIEN) AS KALORIEN
FROM rezept AS R INNER JOIN zutat_rezept ZT ON R.re_id= ZT.rezept_id 
INNER JOIN zutat AS Z ON ZT.zutat_id= Z.ZUTATENNR
GROUP BY R.re_id
HAVING  SUM(Z.KALORIEN) < 500;


/*Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten*/
SELECT R.re_name, R.re_anleitung, COUNT(ZT.zutat_id)
FROM rezept AS R INNER JOIN zutat_rezept ZT ON R.re_id= ZT.rezept_id 
GROUP BY R.re_id
HAVING COUNT(ZT.zutat_id)<5;

/*Auswahl aller Rezepte, die weniger als fünf Zutaten enthalten und eine bestimmte Ernährungskategorie erfüllen*/
SELECT R.re_name,R.re_anleitung
FROM ernaehrungskategorie_rez ER INNER JOIN rezept R ON ER.rezept_id= R.re_id
WHERE ER.ernkat_id=1 AND R.re_id IN (SELECT R.re_id
FROM rezept AS R INNER JOIN zutat_rezept ZT ON R.re_id= ZT.rezept_id 
GROUP BY R.re_id
HAVING COUNT(ZT.zutat_id)<5);


/*drei eigene Abfragen*/


/*rezept und nettopreis davon*/
SELECT R.re_name, SUM(nettopreis) AS Rezeptnettopreis
	FROM zutat 
	JOIN zutat_rezept ZR ON ZR.zutat_id = zutat.ZUTATENNR
	JOIN rezept R ON ZR.rezept_id = R.re_id
GROUP BY R.re_id;

/*alle Zutaten von einem Rezept*/
SELECT Z.BEZEICHNUNG, ZR.menge FROM zutat Z
JOIN zutat_rezept ZR ON ZR.zutat_id= Z.ZUTATENNR
WHERE Z.ZUTATENNR IN (
	SELECT ZR.zutat_id FROM zutat_rezept ZR
	JOIN rezept R ON ZR.rezept_id = R.re_id
	WHERE R.re_name = 'Gemüsesuppe'
	);
/*alle Lieferant von einem rezept*/
SELECT L.LIEFERANTENNAME FROM lieferant L
WHERE LIEFERANTENNR IN (
SELECT Z.LIEFERANT FROM zutat Z 
	JOIN zutat_rezept ZR ON ZR.zutat_id = Z.ZUTATENNR
	JOIN rezept R ON ZR.rezept_id = R.re_id
GROUP BY rezeptid );


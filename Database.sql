-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.4.14-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Exportiere Datenbank Struktur für krautundrueben_test
CREATE DATABASE IF NOT EXISTS `krautundrueben_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `krautundrueben_test`;

-- Exportiere Struktur von Tabelle krautundrueben_test.allergene
CREATE TABLE IF NOT EXISTS `allergene` (
  `al_id` int(11) NOT NULL AUTO_INCREMENT,
  `al_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.allergene: ~5 rows (ungefähr)
/*!40000 ALTER TABLE `allergene` DISABLE KEYS */;
INSERT INTO `allergene` (`al_id`, `al_name`) VALUES
	(1, 'Gluten'),
	(2, 'Laktose'),
	(3, 'Weizen'),
	(4, 'Stärke'),
	(5, 'Karotte');
/*!40000 ALTER TABLE `allergene` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.bestellung
CREATE TABLE IF NOT EXISTS `bestellung` (
  `BESTELLNR` int(11) NOT NULL AUTO_INCREMENT,
  `KUNDENNR` int(11) DEFAULT NULL,
  `BESTELLDATUM` date DEFAULT NULL,
  `RECHNUNGSBETRAG` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`BESTELLNR`),
  KEY `KUNDENNR` (`KUNDENNR`),
  CONSTRAINT `bestellung_ibfk_1` FOREIGN KEY (`KUNDENNR`) REFERENCES `kunde` (`KUNDENNR`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.bestellung: ~13 rows (ungefähr)
/*!40000 ALTER TABLE `bestellung` DISABLE KEYS */;
INSERT INTO `bestellung` (`BESTELLNR`, `KUNDENNR`, `BESTELLDATUM`, `RECHNUNGSBETRAG`) VALUES
	(1, 2001, '2020-07-01', 6.21),
	(2, 2002, '2020-07-08', 32.96),
	(3, 2003, '2020-08-01', 24.08),
	(4, 2004, '2020-08-02', 19.90),
	(5, 2005, '2020-08-02', 6.47),
	(6, 2006, '2020-08-10', 6.96),
	(7, 2007, '2020-08-10', 2.41),
	(8, 2008, '2020-08-10', 13.80),
	(9, 2009, '2020-08-10', 8.67),
	(10, 2007, '2020-08-15', 17.98),
	(11, 2005, '2020-08-12', 8.67),
	(12, 2003, '2020-08-13', 20.87),
	(13, 2001, '2020-09-28', 5.00);
/*!40000 ALTER TABLE `bestellung` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.bestellungzutat
CREATE TABLE IF NOT EXISTS `bestellungzutat` (
  `BESTELLNR` int(11) NOT NULL,
  `ZUTATENNR` int(11) NOT NULL,
  `MENGE` int(11) DEFAULT NULL,
  PRIMARY KEY (`BESTELLNR`,`ZUTATENNR`),
  KEY `ZUTATENNR` (`ZUTATENNR`),
  CONSTRAINT `bestellungzutat_ibfk_1` FOREIGN KEY (`BESTELLNR`) REFERENCES `bestellung` (`BESTELLNR`),
  CONSTRAINT `bestellungzutat_ibfk_2` FOREIGN KEY (`ZUTATENNR`) REFERENCES `zutat` (`ZUTATENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.bestellungzutat: ~27 rows (ungefähr)
/*!40000 ALTER TABLE `bestellungzutat` DISABLE KEYS */;
INSERT INTO `bestellungzutat` (`BESTELLNR`, `ZUTATENNR`, `MENGE`) VALUES
	(1, 1001, 5),
	(1, 1002, 3),
	(1, 1004, 3),
	(1, 1006, 2),
	(2, 1003, 4),
	(2, 1005, 5),
	(2, 6408, 5),
	(2, 9001, 10),
	(3, 3001, 5),
	(3, 6300, 15),
	(4, 3003, 2),
	(4, 5001, 7),
	(5, 1001, 5),
	(5, 1002, 4),
	(5, 1004, 5),
	(6, 1010, 5),
	(7, 1009, 9),
	(8, 1008, 7),
	(8, 1012, 5),
	(9, 1007, 4),
	(9, 1012, 5),
	(10, 1011, 7),
	(10, 4001, 7),
	(11, 1012, 5),
	(11, 5001, 2),
	(12, 1010, 15),
	(13, 3003, 1);
/*!40000 ALTER TABLE `bestellungzutat` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.ernaehrungskategorie
CREATE TABLE IF NOT EXISTS `ernaehrungskategorie` (
  `ern_id` int(11) NOT NULL AUTO_INCREMENT,
  `ern_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ern_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.ernaehrungskategorie: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `ernaehrungskategorie` DISABLE KEYS */;
INSERT INTO `ernaehrungskategorie` (`ern_id`, `ern_name`) VALUES
	(1, 'Vegan'),
	(2, 'Vegetarisch'),
	(3, 'Paleo');
/*!40000 ALTER TABLE `ernaehrungskategorie` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.ernaehrungskategorie_rez
CREATE TABLE IF NOT EXISTS `ernaehrungskategorie_rez` (
  `ernkat_id` int(11) NOT NULL,
  `rezept_id` int(11) NOT NULL,
  PRIMARY KEY (`ernkat_id`,`rezept_id`),
  KEY `rezept_id` (`rezept_id`),
  CONSTRAINT `ernaehrungskategorie_rez_ibfk_1` FOREIGN KEY (`ernkat_id`) REFERENCES `ernaehrungskategorie` (`ern_id`),
  CONSTRAINT `ernaehrungskategorie_rez_ibfk_2` FOREIGN KEY (`rezept_id`) REFERENCES `rezept` (`re_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.ernaehrungskategorie_rez: ~4 rows (ungefähr)
/*!40000 ALTER TABLE `ernaehrungskategorie_rez` DISABLE KEYS */;
INSERT INTO `ernaehrungskategorie_rez` (`ernkat_id`, `rezept_id`) VALUES
	(1, 5),
	(1, 8),
	(2, 4),
	(2, 7);
/*!40000 ALTER TABLE `ernaehrungskategorie_rez` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.kunde
CREATE TABLE IF NOT EXISTS `kunde` (
  `KUNDENNR` int(11) NOT NULL,
  `NACHNAME` varchar(50) DEFAULT NULL,
  `VORNAME` varchar(50) DEFAULT NULL,
  `GEBURTSDATUM` date DEFAULT NULL,
  `STRASSE` varchar(50) DEFAULT NULL,
  `HAUSNR` varchar(6) DEFAULT NULL,
  `PLZ` varchar(5) DEFAULT NULL,
  `ORT` varchar(50) DEFAULT NULL,
  `TELEFON` varchar(25) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KUNDENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.kunde: ~9 rows (ungefähr)
/*!40000 ALTER TABLE `kunde` DISABLE KEYS */;
INSERT INTO `kunde` (`KUNDENNR`, `NACHNAME`, `VORNAME`, `GEBURTSDATUM`, `STRASSE`, `HAUSNR`, `PLZ`, `ORT`, `TELEFON`, `EMAIL`) VALUES
	(2001, 'Wellensteyn', 'Kira', '1990-05-05', 'Eppendorfer Landstrasse', '104', '20249', 'Hamburg', '040/443322', 'k.wellensteyn@yahoo.de'),
	(2002, 'Foede', 'Dorothea', '2000-03-24', 'Ohmstraße', '23', '22765', 'Hamburg', '040/543822', 'd.foede@web.de'),
	(2003, 'Leberer', 'Sigrid', '1989-09-21', 'Bilser Berg', '6', '20459', 'Hamburg', '0175/1234588', 'sigrid@leberer.de'),
	(2004, 'Soerensen', 'Hanna', '1974-04-03', 'Alter Teichweg', '95', '22049', 'Hamburg', '040/634578', 'h.soerensen@yahoo.de'),
	(2005, 'Schnitter', 'Marten', '1964-04-17', 'Stübels', '10', '22835', 'Barsbüttel', '0176/447587', 'schni_mart@gmail.com'),
	(2006, 'Maurer', 'Belinda', '1978-09-09', 'Grotelertwiete', '4a', '21075', 'Hamburg', '040/332189', 'belinda1978@yahoo.de'),
	(2007, 'Gessert', 'Armin', '1978-01-29', 'Küstersweg', '3', '21079', 'Hamburg', '040/67890', 'armin@gessert.de'),
	(2008, 'Haessig', 'Jean-Marc', '1982-08-30', 'Neugrabener Bahnhofstraße', '30', '21149', 'Hamburg', '0178-67013390', 'jm@haessig.de'),
	(2009, 'Urocki', 'Eric', '1999-12-04', 'Elbchaussee', '228', '22605', 'Hamburg', '0152-96701390', 'urocki@outlook.de');
/*!40000 ALTER TABLE `kunde` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.lieferant
CREATE TABLE IF NOT EXISTS `lieferant` (
  `LIEFERANTENNR` int(11) NOT NULL,
  `LIEFERANTENNAME` varchar(50) DEFAULT NULL,
  `STRASSE` varchar(50) DEFAULT NULL,
  `HAUSNR` varchar(6) DEFAULT NULL,
  `PLZ` varchar(5) DEFAULT NULL,
  `ORT` varchar(50) DEFAULT NULL,
  `TELEFON` varchar(25) DEFAULT NULL,
  `EMAIL` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`LIEFERANTENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.lieferant: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `lieferant` DISABLE KEYS */;
INSERT INTO `lieferant` (`LIEFERANTENNR`, `LIEFERANTENNAME`, `STRASSE`, `HAUSNR`, `PLZ`, `ORT`, `TELEFON`, `EMAIL`) VALUES
	(101, 'Bio-Hof Müller', 'Dorfstraße', '74', '24354', 'Weseby', '04354-9080', 'mueller@biohof.de'),
	(102, 'Obst-Hof Altes Land', 'Westerjork 74', '76', '21635', 'Jork', '04162-4523', 'info@biohof-altesland.de'),
	(103, 'Molkerei Henning', 'Molkereiwegkundekunde', '13', '19217', 'Dechow', '038873-8976', 'info@molkerei-henning.de');
/*!40000 ALTER TABLE `lieferant` ENABLE KEYS */;

-- Exportiere Struktur von Prozedur krautundrueben_test.naehrwerte_kunde
DELIMITER //
CREATE PROCEDURE `naehrwerte_kunde`(k_id INT)
BEGIN 
SELECT AVG(Z.KALORIEN) AS Durschnittliche_Kalorien ,AVG(Z.KOHLENHYDRATE) AS Durschnittliche_Kalorien , AVG (Z.PROTEIN) AS Durschnittliche_Protein
FROM kunde AS K INNER JOIN bestellung AS B ON K.KUNDENNR= B.KUNDENNR 
 join bestellungzutat AS BT ON B.BESTELLNR= BT.BESTELLNR
 JOIN zutat AS Z ON Z.ZUTATENNR=BT.ZUTATENNR
 WHERE K.KUNDENNR= k_id;
END//
DELIMITER ;

-- Exportiere Struktur von Tabelle krautundrueben_test.rezept
CREATE TABLE IF NOT EXISTS `rezept` (
  `re_id` int(11) NOT NULL AUTO_INCREMENT,
  `re_name` varchar(50) DEFAULT NULL,
  `re_anleitung` text DEFAULT NULL,
  PRIMARY KEY (`re_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.rezept: ~8 rows (ungefähr)
/*!40000 ALTER TABLE `rezept` DISABLE KEYS */;
INSERT INTO `rezept` (`re_id`, `re_name`, `re_anleitung`) VALUES
	(1, 'Lachslasagne', 'Den Spinat 5 min vorgaren, auspressen bis er ganz trocken ist und würzen. \r\nDen Lachs etwas auftauen lassen, in kleine Stücke schneiden und mit Zitrone, Salz und Pfeffer würzen. \r\nFür die Bechamel-Soße wird die Butter im Topf erhitzt, das Mehl verrührt und mit der Milch aufgegossen. Die Brühe dazugeben und 5 min. kochen lassen. Danach die Sahne dazurühren und nochmals aufkochen lassen.\r\nSchließlich den Parmesan hinzufügen und mit Pfeffer und Muskat abschmecken. Jetzt alles abwechselnd in eine feuerfeste Form schichten, zuerst die Soße, dann Lasagneblätter, Soße, Spinat, Lachs, Käse, Lasagneblätter, usw. Letzte Schicht ist Soße.\r\nDie Lasagne anschließend bei 200 Grad 45 min. in den Backofen.\r\n'),
	(2, 'Thaicurry mit Hähnchen', 'Die Hühnerbrust in Streifen schneiden und in einer Marinade aus Sojasauce, Pfeffer und Zucker ca. 1 Stunde marinieren.\r\nIm Wok oder in der Pfanne gut anbraten. Herausnehmen.\r\nGemischtes Gemüse und Bambussprossen waschen und sehr fein schneiden, ebenfalls anbraten, dann herausnehmen. \r\nDie Currypaste und die Kokosmilch im Wok verrühren und kurz (!) durchköcheln lassen. Dann Fleisch und Gemüse wieder zugeben und noch kurz köcheln lassen. \r\nMit Sojasauce abschmecken.\r\nIn der Zwischenzeit den Reis kochen und abtropfen lassen.\r\n'),
	(3, 'Kartoffelsuppe', 'Kartoffeln und Karotte schälen und in grobe Würfel schneiden. Zwiebel würfeln. Lauch in Ringe schneiden.\r\nZwiebeln in etwas Olivenöl glasig dünsten, Lauch dazugeben. Mit der Gemüsebrühe ablöschen. \r\nDie gewürfelten Kartoffeln und Karotte in die Brühe geben. Alles für ca. 20 min köcheln lassen.\r\nAnschließend alles mit dem Pürierstab zu einer Suppe pürieren, mit Pfeffer und Muskat abschmecken.\r\nDie in Scheiben geschnittenen Würstchen in den Suppenteller geben und die heiße Kartoffelsuppe darüber gießen\r\n'),
	(4, 'Milchreis mit Apfelmus', 'In einem großen Topf die Butter schmelzen, anschließend den Rundkornreis kurz in der Butter anschwitzen. Nun die zimmerwarme Vollmilch sowie 4 EL Zucker hinzugeben. Vanillezucker hinzugeben. Alles unter vorsichtigem Rühren mit dem Holzkochlöffel einmal aufkochen lassen und dabei aufpassen, dass sich nichts am Topfboden ansetzt.\r\n\r\nNun den Topf auf eine Herdplatte stellen, die auf kleinster Stufe heizt. Den Milchreis im geschlossenen Topf exakt 30 Minuten ziehen lassen. Nach der Hälfte der Zeit einmal umrühren.\r\n\r\nNach 30 Minuten ist der Milchreis servierfertig, er kann warm oder kalt gegessen werden.\r\n\r\nNach Geschmack Apfelmus dazu reichen.\r\n\r\n'),
	(5, 'Sommerlicher Couscous-Salat', 'Couscous in eine Schale geben und mit kochender Gemüsebrühe übergießen. Kurz umrühren, quellen lassen und evtl. Wasser nachgeben. \r\nInzwischen das Gemüse waschen und anschließend würfeln. Die Kräuter waschen und kleinhacken. Diese Zutaten mit dem Couscous vermischen. Nun Öl hinzugeben und zum Schluss mit Salz und Pfeffer abschmecken.\r\n'),
	(7, 'Tomatensauce', 'bla bla bla'),
	(8, 'Gemüsesuppe', 'bli bli bli'),
	(9, 'Wienercouscoussalat', 'bla bla bla');
/*!40000 ALTER TABLE `rezept` ENABLE KEYS */;

-- Exportiere Struktur von Prozedur krautundrueben_test.rezept_kat
DELIMITER //
CREATE PROCEDURE `rezept_kat`(ernkat INT)
BEGIN 

SELECT R.re_name, R.re_anleitung 
FROM rezept AS R INNER JOIN  ernaehrungskategorie_rez AS ER ON R.re_id= ER.rezept_id
WHERE ER.ernkat_id=ernkat; 

END//
DELIMITER ;

-- Exportiere Struktur von Prozedur krautundrueben_test.re_5Z_ER
DELIMITER //
CREATE PROCEDURE `re_5Z_ER`(erkat INT)
BEGIN
SELECT R.re_name,R.re_anleitung
FROM ernaehrungskategorie_rez ER INNER JOIN rezept R ON ER.rezept_id= R.re_id
WHERE ER.ernkat_id=erkat AND R.re_id IN (SELECT R.re_id
FROM rezept AS R INNER JOIN zutat_rezept ZT ON R.re_id= ZT.rezept_id 
GROUP BY R.re_id
HAVING COUNT(ZT.zutat_id)<5);

END//
DELIMITER ;

-- Exportiere Struktur von Tabelle krautundrueben_test.zutat
CREATE TABLE IF NOT EXISTS `zutat` (
  `ZUTATENNR` int(11) NOT NULL,
  `BEZEICHNUNG` varchar(50) DEFAULT NULL,
  `EINHEIT` varchar(25) DEFAULT NULL,
  `NETTOPREIS` decimal(10,2) DEFAULT NULL,
  `BESTAND` int(11) DEFAULT NULL,
  `LIEFERANT` int(11) DEFAULT NULL,
  `KALORIEN` int(11) DEFAULT NULL,
  `KOHLENHYDRATE` decimal(10,2) DEFAULT NULL,
  `PROTEIN` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`ZUTATENNR`),
  KEY `LIEFERANT` (`LIEFERANT`),
  CONSTRAINT `zutat_ibfk_1` FOREIGN KEY (`LIEFERANT`) REFERENCES `lieferant` (`LIEFERANTENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.zutat: ~23 rows (ungefähr)
/*!40000 ALTER TABLE `zutat` DISABLE KEYS */;
INSERT INTO `zutat` (`ZUTATENNR`, `BEZEICHNUNG`, `EINHEIT`, `NETTOPREIS`, `BESTAND`, `LIEFERANT`, `KALORIEN`, `KOHLENHYDRATE`, `PROTEIN`) VALUES
	(1001, 'Zucchini', 'Stück', 0.89, 100, 101, 19, 2.00, 1.60),
	(1002, 'Zwiebel', 'Stück', 0.15, 50, 101, 28, 4.90, 1.20),
	(1003, 'Tomate', 'Stück', 0.45, 50, 101, 18, 2.60, 1.00),
	(1004, 'Schalotte', 'Stück', 0.20, 500, 101, 25, 3.30, 1.50),
	(1005, 'Karotte', 'Stück', 0.30, 500, 101, 41, 10.00, 0.90),
	(1006, 'Kartoffel', 'Stück', 0.15, 1500, 101, 71, 14.60, 2.00),
	(1007, 'Rucola', 'Bund', 0.90, 10, 101, 27, 2.10, 2.60),
	(1008, 'Lauch', 'Stück', 1.20, 35, 101, 29, 3.30, 2.10),
	(1009, 'Knoblauch', 'Stück', 0.25, 250, 101, 141, 28.40, 6.10),
	(1010, 'Basilikum', 'Bund', 1.30, 10, 101, 41, 5.10, 3.10),
	(1011, 'Süßkartoffel', 'Stück', 2.00, 200, 101, 86, 20.00, 1.60),
	(1012, 'Schnittlauch', 'Bund', 0.90, 10, 101, 28, 1.00, 3.00),
	(2001, 'Apfel', 'Stück', 1.20, 750, 102, 54, 14.40, 0.30),
	(3001, 'Vollmilch. 3.5%', 'Liter', 1.50, 50, 103, 65, 4.70, 3.40),
	(3002, 'Mozzarella', 'Packung', 3.50, 20, 103, 241, 1.00, 18.10),
	(3003, 'Butter', 'Stück', 3.00, 50, 103, 741, 0.60, 0.70),
	(4001, 'Ei', 'Stück', 0.40, 300, 102, 137, 1.50, 11.90),
	(5001, 'Wiener Würstchen', 'Paar', 1.80, 40, 101, 331, 1.20, 9.90),
	(5002, 'Hähnchen', 'Packung', 0.15, 50, 101, 28, 4.90, 1.20),
	(6300, 'Kichererbsen', 'Dose', 1.00, 400, 103, 150, 21.20, 9.00),
	(6408, 'Couscous', 'Packung', 1.90, 15, 102, 351, 67.00, 12.00),
	(7043, 'Gemüsebrühe', 'Würfel', 0.20, 4000, 101, 1, 0.50, 0.50),
	(9001, 'Tofu-Würstchen', 'Stück', 1.80, 20, 103, 252, 7.00, 17.00);
/*!40000 ALTER TABLE `zutat` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.zutat_allergene
CREATE TABLE IF NOT EXISTS `zutat_allergene` (
  `allergene_id` int(11) NOT NULL,
  `zutat_id` int(11) NOT NULL,
  PRIMARY KEY (`allergene_id`,`zutat_id`),
  KEY `zutat_id` (`zutat_id`),
  CONSTRAINT `zutat_allergene_ibfk_1` FOREIGN KEY (`allergene_id`) REFERENCES `allergene` (`al_id`),
  CONSTRAINT `zutat_allergene_ibfk_2` FOREIGN KEY (`zutat_id`) REFERENCES `zutat` (`ZUTATENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.zutat_allergene: ~3 rows (ungefähr)
/*!40000 ALTER TABLE `zutat_allergene` DISABLE KEYS */;
INSERT INTO `zutat_allergene` (`allergene_id`, `zutat_id`) VALUES
	(1, 6408),
	(2, 3001),
	(2, 3002);
/*!40000 ALTER TABLE `zutat_allergene` ENABLE KEYS */;

-- Exportiere Struktur von Tabelle krautundrueben_test.zutat_rezept
CREATE TABLE IF NOT EXISTS `zutat_rezept` (
  `rezept_id` int(11) NOT NULL,
  `zutat_id` int(11) NOT NULL,
  `menge` int(11) DEFAULT NULL,
  PRIMARY KEY (`rezept_id`,`zutat_id`),
  KEY `zutat_id` (`zutat_id`),
  CONSTRAINT `zutat_rezept_ibfk_1` FOREIGN KEY (`rezept_id`) REFERENCES `rezept` (`re_id`),
  CONSTRAINT `zutat_rezept_ibfk_2` FOREIGN KEY (`zutat_id`) REFERENCES `zutat` (`ZUTATENNR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Exportiere Daten aus Tabelle krautundrueben_test.zutat_rezept: ~13 rows (ungefähr)
/*!40000 ALTER TABLE `zutat_rezept` DISABLE KEYS */;
INSERT INTO `zutat_rezept` (`rezept_id`, `zutat_id`, `menge`) VALUES
	(7, 1003, 3),
	(7, 3001, 1),
	(7, 3002, 100),
	(8, 1005, 1),
	(8, 1006, 1),
	(8, 1008, 1),
	(8, 7043, 1),
	(9, 3001, 1),
	(9, 3002, 1),
	(9, 3003, 1),
	(9, 4001, 1),
	(9, 5001, 1),
	(9, 6408, 1);
/*!40000 ALTER TABLE `zutat_rezept` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
